import fontforge
import os

FOLDER = "static/fonts/"

in_exts = ['.svg']
out_ext = '.otf'

verbose = True


# GLYPH HANDLING
# =========================================================================


def redraw_contours(glyph, new_contours):
    # redraw the glyph according to the new contour
    # new_contours must be a list of contours

    if verbose: print('--redraw')

    pen = glyph.glyphPen()
    # > By default the pen will replace any existing contours and references, 
    # so because it empty the contour, we have to put them somewhere else so they're not deleted
    # i.e. in new_contours
    for new_contour in new_contours:
        new_contour.draw(pen)

    if verbose: print('done')


def single2double_contours(contour):
    # take a single contour and trace it back 
    # to return a doubled contour

    if len(contour) == 2:
        if verbose: print('fixing 2 points contour by adding a 3rd,')
        # then we know both point must be 'on' the curve
        mid_x = (contour[0].x + contour[1].x)/2
        mid_y = (contour[0].y + contour[1].y)/2
        mid_point = fontforge.point(mid_x, mid_y, True)
        contour.insertPoint(mid_point, 0)

    # things like an O might already be closed
    if contour.closed:
        if verbose: print('contour already closed.')

    # on some font it's possible that we have a contour
    # forming a loop but not properly closed as a path
    # if we leave it like that we'll end up make a double line like for the other glyphs
    # while we could have simply properly cose it
    elif contour[0] == contour[-1]:
        if verbose: print('closing contour with already same start and end points.')
        contour.closed = 1

    else:
        if verbose: print('doubling a contour.')
        # NOTE: we can not only reverse the list,
        # otherwise point and anchor are not correctly associated anymore
        reversed_contour = contour[::-1]
        # but this one do the exact same thing:
        # contour.reverseDirection()
        merged_contour = contour[:-1] + reversed_contour
        merged_contour.closed = 1
        contour = merged_contour

    return contour
    

def convert_glyph(glyph):

    if verbose: print('=== ' + glyph.glyphname + ' ===')
    glyph_width = glyph.width

    if verbose: print('--contours')
    new_contours = []

    # layer[1] contains the foreground glyphs contours
    # see https://dmtr.org/ff.php#Contour
    # to understand what's a contour
    for contour in glyph.layers[1]:

        contour = single2double_contours(contour)
        new_contours.append(contour)

    redraw_contours(glyph, new_contours)

    # NOTE: is it neccessary?
    old_layer = glyph.layers[1].dup()
    glyph.layers[1].correctDirection()
    if glyph.layers[1] != old_layer:
        if verbose: print('--correction')
        if verbose: print('layer[1] direction corrected')

    # NOTE: it seems redrawing reset glyph.width at em square...
    # can we make sure we didn't lost other stuff?
    glyph.width = glyph_width
    if verbose: print('width', glyph.width)
    # print(glyph.left_side_bearing)
    # print(glyph.right_side_bearing)


def check_font(font):
    # check if every contour is closed!
    for glyph in font.glyphs():
        is_glyph_closed = True

        for contour in glyph.layers[1]:
            if not contour.closed:
                is_glyph_closed = False

        if not is_glyph_closed:
            print('WARNING', glyph.glyphname, ' is not closed...')



# FONT HANDLING
# =========================================================================


def convert_font(PATH):

    (root, filename) = os.path.split(PATH)
    (basename, ext) = os.path.splitext(filename)
    PATH_OUT = os.path.join(root, basename + out_ext)
    
    font = fontforge.open(PATH)
    print('###', filename, '###')

    # print some layer info
    # NOTE: is it possible that a font contains other layer
    # mixed between color font and svg single line?
    # and we not erased the color font layer, so we end up
    # with double stroke on chrome and single on firefox
    # because of their different support
    print('-- font-layers')
    for layer in font.layers:
        print(layer, '| is quadratic', font.layers[layer].is_quadratic)

    # iterate over glyphs
    for glyph in font.glyphs():
        convert_glyph(glyph)

    # check if every path is closed now
    check_font(font)

    # save back to ttf
    font.generate(PATH_OUT)
    print('### converted ###')

    # re-open and compare to see of generate did not modify stuffs
    font_exported = fontforge.open(PATH_OUT)
    if verbose: font.compareFonts(font_exported, '-', ('outlines', 'outlines-exactly'))



def convert_fonts(FOLDER):

    # iterate through file in PATH
    for root, dirs, files in os.walk(FOLDER):
        for filename in files:

            # if svg font then get name and open it
            (basename, ext) = os.path.splitext(filename)
            if ext in in_exts:

                PATH = os.path.join(root, filename)
                convert_font(PATH)


# MAIN
# =========================================================================

if __name__ == '__main__':
    convert_fonts(FOLDER)

