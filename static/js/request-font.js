
let d = document;

let select_font = d.getElementById("select-font");

function pick_font(name){
    let els = d.querySelectorAll('.stroke-font');
    for(el of els){
        console.log(el, name);
        el.style.fontFamily = name;
        // el.style.color = 'red';
    }
}

select_font.addEventListener("change", function(){
    pick_font(this.value);
});

pick_font(select_font.value);