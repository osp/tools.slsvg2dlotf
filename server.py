from flask import Flask
from flask import render_template
import os

app = Flask("css-stroke-font")

FONTS_PATH = "static/fonts/"
possible_extensions = [".ttf", ".otf"]

@app.route("/")
def index():
    fonts = []
    for root, dirs, files in os.walk(FONTS_PATH):
        for filename in files:
            (basename, ext) = os.path.splitext(filename)
            if ext in possible_extensions:
                print(" * " + os.path.join(root,filename))
                fonts.append({ 'name': basename, 'path': os.path.join(root,filename)})

    return render_template('index.html', fonts=fonts)
